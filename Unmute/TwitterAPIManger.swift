//
//  TwitterAPIManger.swift
//  Unmute
//
//  Created by Dennis on 11/26/17.
//  Copyright © 2017 RudoyApp. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterAPIManger: NSObject {
    
    class func getFeedForCurrentUser(userId: String?, completion: @escaping ([TWTRTweet]?, Error?) -> Void) {
        
        if let userId = userId {
            let client = TWTRAPIClient.init(userID: userId)
            
            let request = client.urlRequest(withMethod: "GET", url: "https://api.twitter.com/1.1/statuses/user_timeline.json", parameters: ["user_id":userId], error: nil)
            
            
            let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in

                if (error != nil) {
                    DispatchQueue.main.async {
                        completion(nil,error)
                    }
                } else {
                        DispatchQueue.main.async {
                            if let data = data {
                                let json = TwitterAPIManger.nsdataToJSON(data: data)
                                let tweets:[TWTRTweet] = TWTRTweet.tweets(withJSONArray: json) as! [TWTRTweet]
                            completion(tweets,nil)
                        }
                    }
                }
                
            }
            task.resume()
        }
        
        
        
    }
    
    class func nsdataToJSON(data: Data) -> [Any]? {
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as? [Any]
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    

}
