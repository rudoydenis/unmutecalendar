//
//  CalendarVC.swift
//  Unmute
//
//  Created by Dennis on 11/26/17.
//  Copyright © 2017 RudoyApp. All rights reserved.
//

import UIKit
import FSCalendar
import TwitterKit


class CalendarVC: UIViewController, FSCalendarDelegate, FSCalendarDataSource {
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var tweetView: TWTRTweetView!
    var impactGenerator = UIImpactFeedbackGenerator.init(style:.heavy)
    var tweets:[TWTRTweet]?{
        didSet{
            DispatchQueue.main.async {
                self.calendar.reloadData()
            }
        }
    }
    
    var twitterSession:TWTRSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tweetView.isHidden = true
        
        if let userId = twitterSession?.userID{
            TwitterAPIManger.getFeedForCurrentUser(userId: userId, completion: { (tweets, error) in
                self.tweets = tweets
            })
        }

        let showListTimeButton = UIBarButtonItem(title: "My Posts", style: .plain, target: self, action: #selector(showListTimelineVC))
        navigationItem.rightBarButtonItems = [showListTimeButton]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
  
    @objc func showListTimelineVC()  {
        let listTimelineVC = ListTimelineViewController()
        listTimelineVC.screenName = twitterSession?.userName
        navigationController?.pushViewController(listTimelineVC, animated: true)
    }
    
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        var numberOfPosts = 0
        if let tweets = tweets {
            for tweet in tweets {
                if Calendar.current.isDate(date, inSameDayAs:tweet.createdAt) {
                    numberOfPosts = numberOfPosts + 1
                }
            }
            return numberOfPosts
        } else {
            return 0
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        impactGenerator.impactOccurred()
        if let tweets = tweets {
            var tweetExists:Bool = false
            for tweet in tweets {
                if Calendar.current.isDate(date, inSameDayAs:tweet.createdAt) {
                    self.tweetView.configure(with: tweet)
                    tweetExists = true
                }
            }
            tweetView.isHidden = !tweetExists
        } else {
            tweetView.isHidden = true
        }
    }
}
