//
//  LoginVC.swift
//  Unmute
//
//  Created by Dennis on 11/26/17.
//  Copyright © 2017 RudoyApp. All rights reserved.
//

import UIKit
import TwitterKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
            if (session != nil) {
                print("signed in as \(session?.userName ?? "nil")");
            } else {
                print("error: \(error?.localizedDescription ?? "nil")");
            }
        })
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)
        
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(session?.userName ?? "nil")");
                self.launchCalendar(twitterSession: session)
                
            } else {
                print("error: \(error?.localizedDescription ?? "nil")");
            }
        })
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func launchCalendar(twitterSession:TWTRSession?) {
        let calendarVC:CalendarVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        calendarVC.twitterSession = twitterSession
        let navController = UINavigationController.init(rootViewController: calendarVC)
        navController.navigationBar.isTranslucent = false
        navController.modalTransitionStyle = .flipHorizontal
        present(navController, animated: true, completion: nil)
    }

   

}
