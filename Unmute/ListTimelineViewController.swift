//
//  ListTimelineViewController.swift
//  Unmute
//
//  Created by Dennis on 11/26/17.
//  Copyright © 2017 RudoyApp. All rights reserved.
//

import Foundation
import UIKit
import TwitterKit


class ListTimelineViewController: TWTRTimelineViewController {
    
    var screenName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let screenName = screenName {
            self.dataSource = TWTRUserTimelineDataSource(screenName: screenName, apiClient: TWTRAPIClient())
        }
        
    }
}

